//
//  GalleryViewCell.swift
//  test
//
//  Created by Devnir on 15.12.17.
//  Copyright © 2017 devnir. All rights reserved.
//

import UIKit

class GalleryViewCell: UICollectionViewCell {

    @IBOutlet weak var galeryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
