//
//  ViewController.swift
//  test
//
//  Created by Devnir on 15.12.17.
//  Copyright © 2017 devnir. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
import Alamofire
import Foundation


class ViewController: UIViewController {
    let netManager: NetManager = NetManager()
    let locationManager = CLLocationManager()
    let searchBar = UISearchBar()

    @IBOutlet weak var galleryView: UICollectionView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var radiusLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        
        galleryView.delegate = self
        galleryView.dataSource = self
        galleryView.register(UINib(nibName: "GalleryViewCell", bundle: nil), forCellWithReuseIdentifier: "galleryCell")
        
        radiusLabel.text = "Search radius: \(radiusSlider.value) m"
        radiusSlider.value = Constants.Defaults.searchRadius
        self.setupUserTrackingButton()
        if CLLocationManager.authorizationStatus() == .notDetermined
        {
            locationManager.requestWhenInUseAuthorization()
        }
        searchBar.delegate = self
        searchBar.sizeToFit()
        navigationItem.titleView = searchBar
        
        mapView.delegate = self
        mapView.register(MarkerView.self, forAnnotationViewWithReuseIdentifier: "marker")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(searchPlacesResultHundler(notification:)),
                                               name: NSNotification.Name(rawValue: Constants.NotificationsId.searchPlacesResult),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(downloadPlacesImageHundler(notification:)),
                                               name: NSNotification.Name(rawValue: Constants.NotificationsId.downloadImageResult),
                                               object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    @objc func downloadPlacesImageHundler(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let status  = userInfo["status"] as? String else {
                self.showAlert(title: "Download image error", msg: "No userInfo or request status found in notification");
                return
        }
        if status == "OK" {
            self.galleryView.reloadData()
            UIView.animate(withDuration: 0.7, animations: {
                self.galleryView.alpha = 1
            })
        }else{
            guard let error = userInfo["error"] as? String else{
                self.showAlert(title: "Download image error", msg: "Undefined error");
                return
            }
            self.showAlert(title: "Download image error", msg: error);
            return
        }
    }
    
    
    @objc func searchPlacesResultHundler(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let status  = userInfo["status"] as? String else {
                self.showAlert(title: "Search error", msg: "No userInfo or request status found in notification");
                return
        }
        if status == "OK" {
            guard let result = userInfo["places"] as? [GApiSearchResult] else{
                guard let error = userInfo["error"] as? String else{
                    self.showAlert(title: "Search error", msg: "No places found");
                    return
                }
                self.showAlert(title: "Search error", msg: error);
                return
            }
            self.putItemToMap(items: result)
        }else{
            guard let error = userInfo["error"] as? String else{
                self.showAlert(title: "Search error", msg: "Undefined error");
                return
            }
            self.showAlert(title: "Search error", msg: error);
            return
        }
    }
    
    func showAlert(title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        searchBar.endEditing(true)
    }
    

    
    func setupUserTrackingButton() {
        let button = MKUserTrackingButton(mapView: mapView)
        button.layer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        mapView.addSubview(button)
        
        NSLayoutConstraint.activate([button.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 0),
                                     button.leadingAnchor.constraint(equalTo: mapView.leadingAnchor, constant: 5),
                                     ])
    }
    
    func putItemToMap(items:[GApiSearchResult]){
        for item in items{
            let point = MarkerPoint()
            point.coordinate.latitude   = CLLocationDegrees(item.geometry.location.lat)
            point.coordinate.longitude  = CLLocationDegrees(item.geometry.location.lng)
            point.title = item.name
            point.subtitle = item.formatted_address
            point.placeId = item.place_id
            mapView.addAnnotation(point)
        }
    }
   
    @IBAction func radiusChanged(_ sender: Any) {
        radiusLabel.text = "Search radius: \(radiusSlider.value) m"
    }
}

extension ViewController:  CLLocationManagerDelegate, UISearchBarDelegate, MKMapViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.netManager.getGalleryImagesCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath) as! GalleryViewCell
        if let image = self.netManager.getGalleryImage(at: indexPath.row) {
            cell.galeryImage.image = image
        }
        return cell
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        mapView.removeAnnotations(mapView.annotations)
        netManager.loadPoints(keywords: searchBar.text!, location: (locationManager.location?.coordinate)!, radius: radiusSlider.value)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let anot = view.annotation as? MarkerPoint{
            netManager.loadGalery(placeId: anot.placeId, size: self.galleryView.frame.size)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.annotation is MarkerPoint{
            UIView.animate(withDuration: 0.7, animations: {
                self.galleryView.alpha = 0
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}

