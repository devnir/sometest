//
//  MarkerView.swift
//  test
//
//  Created by Devnir on 15.12.17.
//  Copyright © 2017 devnir. All rights reserved.
//

import Foundation
import MapKit

class MarkerView: MKMarkerAnnotationView {    
    override var annotation: MKAnnotation? {
        willSet {
                animatesWhenAdded = false
                titleVisibility = .adaptive
                subtitleVisibility = .hidden
                isDraggable = false
                collisionMode = .circle
                canShowCallout = true
        }
    }
}
