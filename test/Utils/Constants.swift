//
//  Constants.swift
//  test
//
//  Created by Devnir on 20.12.17.
//  Copyright © 2017 devnir. All rights reserved.
//

import Foundation
class Constants {
    struct Defaults{
        static let searchRadius:Float = 5000.0 //(meters)
    }
    
    struct GoogleApi{
        static let key:String   = "AIzaSyA2EEha1v4ZLgaXqoSP4MmNgBooRPClqTU"
    }
    
    struct NotificationsId{
        static let searchPlacesResult  = "NC_SEARCH_PLACES_RESULT_ID"
        static let downloadImageResult = "NC_DOWNLOAD_IMAGE_RESULT_ID"
    }
}
