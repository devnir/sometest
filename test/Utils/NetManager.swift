//
//  Utils.swift
//  test
//
//  Created by Devnir on 20.12.17.
//  Copyright © 2017 devnir. All rights reserved.
//

import Foundation
import MapKit
import GooglePlaces
import Alamofire

class NetManager {
    var placesClient: GMSPlacesClient!
    private var gallery: [UIImage] = []
   
    init() {
        GMSPlacesClient.provideAPIKey(Constants.GoogleApi.key)
        placesClient = GMSPlacesClient.shared()
    }
    
    
    func loadPoints(keywords: String, location: CLLocationCoordinate2D, radius: Float){
        let reqUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(keywords)&location=\(location.latitude),\(location.longitude)&radius=\(radius)&key=\(Constants.GoogleApi.key)"
        
        Alamofire.request(reqUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!).validate().responseJSON(){ response in
            switch response.result {
            case .success:
                let res = response.data!
                let decoder = JSONDecoder()
                do {
                    let apiResult = try decoder.decode(GApiResult.self, from: res)
                    NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.searchPlacesResult),
                                                    object: nil,
                                                    userInfo: ["status":apiResult.status, "places":apiResult.results])
                } catch let e{
                    NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.searchPlacesResult),
                                                    object: nil,
                                                    userInfo: ["status":"ERROR", "error": e.localizedDescription])
                }
            case .failure(let error):
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.searchPlacesResult),
                                                object: nil,
                                                userInfo: ["status":"ERROR", "error": error.localizedDescription])
            }
        }
    }
    
    func loadGalery(placeId: String, size: CGSize){
        self.gallery.removeAll()
        self.placesClient.lookUpPhotos(forPlaceID: placeId, callback: { (metadata, error) in
            if let metadata = metadata {
                let photos: [GMSPlacePhotoMetadata] = metadata.results
                for photo in photos{
                    self.placesClient.loadPlacePhoto(photo, constrainedTo: size, scale: 1) { (image, error) in
                        if let image = image {
                            self.gallery.append(image)
                            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.downloadImageResult), object: nil, userInfo: ["status":"OK"])
                        } else if let error = error {
                            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.downloadImageResult), object: nil, userInfo: ["status":"ERROR", "error": error.localizedDescription])
                        } else {
                            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.downloadImageResult), object: nil, userInfo: ["status":"ERROR", "error": "An unexpected error occured"])
                        }
                    }
                }
            }else if let error = error {
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.downloadImageResult), object: nil, userInfo: ["status":"ERROR", "error": error.localizedDescription])
            } else {
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationsId.downloadImageResult), object: nil, userInfo: ["status":"ERROR", "error": "An unexpected error occured, while download place image"])
            }
        })
    }
    
    func getGalleryImagesCount() -> Int{
        return self.gallery.count
    }
    
    func getGalleryImage(at:Int) -> UIImage?{
        if( 0...gallery.count ~= at){
            return self.gallery[at]
        }
        return nil
    }
}
