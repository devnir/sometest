//
//  DataTypes.swift
//  test
//
//  Created by Devnir on 20.12.17.
//  Copyright © 2017 devnir. All rights reserved.
//

import Foundation

struct GApiLocation:Codable {
    let lat:Float
    let lng:Float
}

struct GApiGeometry:Codable {
    let location:GApiLocation
}

struct GApiSearchResult:Codable {
    let formatted_address:String
    let place_id:String
    let geometry:GApiGeometry
    let name:String
}

struct GApiResult:Codable{
    let status:String
    let results:[GApiSearchResult]
}
